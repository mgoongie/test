<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <div class="center">
    <form action="add.php" method="post">
        <label for="name">Name</label> <input class="form-control width" type="text" name="product_name" id="name"> <br>
        <label for="name">Brand</label><input class="form-control width" type="text" name="product_brand"> <br>
        <label for="name">Color</label><input class="form-control width" type="text" name="color"> <br>
        <label for="name">Price</label><input class="form-control width" type="number" name="price"> <br>
        <input type="submit" value="save" name="save" class="btn btn-info save">
    </form>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>